#! /bin/bash
#Streaming using Shell.
# Source of the stream its may be txt file containing mp3 files 
StreamSource="input.txt"
#Streaming URLs                  
StreamingURL="< enter Streaming URL >" 
#Enter Your Streaming Key
StreamingKey="< enter Streaming Key >" 

ffmpeg  \
 -stream_loop -1 -re \
 -f concat -i $StreamSource \
 -map 0:a -shortest \
 -f flv "$StreamingURL/$StreamingKey"

