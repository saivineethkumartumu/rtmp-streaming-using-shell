#! /bin/bash
#Streaming using Shell.
InputImage=
# Source of the stream its may be txt file containing mp3 files 
StreamSource="input.txt"
#Streaming URLs                  
StreamingURL="< enter Streaming URL >" 
#Enter Your Streaming Key
StreamingKey="< enter Streaming Key >" 


ffmpeg  \
-stream_loop -1 \
-re \
-i $InputImage -vcodec mpeg4 -f mpegts \
-thread_queue_size 512 \
-stream_loop -1 \
-f concat -i $StreamSource \
-map 0:v -map 1:a -shortest \
-vcodec libx264 -pix_fmt yuv420p -preset $QUAL -r $FPS -g $(($FPS * 2)) -b:v> -acodec libmp3lame -ar 44100 -threads 6 -qscale 3 -b:a 712000 -bufsize 512k \
-f flv "$StreamingURL/$StreamingKey" 
